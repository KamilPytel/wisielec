import requests
from stale import API_URL, WIKI_URL

PARAMS = {
    "action": "query",
    "format": "json",
    "list": "random",
    "rnlimit": "1",
    "rnnamespace": 0  # Strony artykułów.
    }


def losuj_hasło():
    '''Zwraca tytuł losowego artykułu
    z polskiej Wikipedii i jego adres URL.'''
    r = requests.get(url=API_URL, params=PARAMS)
    strona = r.json()
    losowa = strona['query']['random'][0]
    id_strony, tytuł = losowa['id'], losowa['title']
    adres_url = '{}?curid={}'.format(WIKI_URL, id_strony)
    return tytuł, adres_url
